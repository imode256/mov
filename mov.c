#include <ncurses.h>
#include <stdio.h>

void program_counter(unsigned *memory) {
    unsigned source;
    unsigned destination;
    if(memory[0] >= 256) {
        memory[0] = 0;
    }
    source      = memory[memory[0]];
    destination = memory[memory[0] + 1];
    memory[destination] = memory[source];
    if(destination != 0) {
        memory[0] += 2;
    }
    return;
}

void alu(unsigned *memory) {
    switch(memory[100]) {
        case 1: {
            memory[103] = memory[101] + memory[102];
            break;
        }
        case 2: {
            memory[103] = memory[101] - memory[102];
            break;
        }
        case 3: {
            memory[103] = memory[101] < memory[102];
            break;
        }
        case 4: {
            memory[104] = memory[101] == memory[102];
            break;
        }
        case 5: {
            memory[105] = memory[101] > memory[102];
            break;
        }
        default: break;
    }
    return;
}

void dereference(unsigned *memory) {
    memory[51] = memory[memory[50]];
    return;
}

void branch(unsigned *memory) {
    if(memory[220] != 0) {
        switch(memory[221]) {
            case 0: {
                memory[0] = memory[222];
            }
            case 1: {
                if(memory[223] != 0) {
                    memory[0] = memory[222];
                }
                break;
            }
            case  2: {
                if(memory[223] == 0) {
                    memory[0] = memory[222];
                }
                break;
            }
            default: break;
        }
        memory[220] = 0;
    }
    return;
}

void print(unsigned *memory) {
    unsigned iterator;
    if(memory[200] != 0) {
        for(iterator = memory[201]; iterator != memory[202]; iterator++) {
            printf("%c", memory[iterator]);
        }
        memory[200] = 0;
    }
    return;
}

void input(unsigned *memory) {
    unsigned value;
    if(memory[240] != 0) {
        scanf("%d", &value);
        memory[241] = value;
        memory[240] = 0;
    }
    return;
}

void process_file(char* filename, unsigned *memory) {
    unsigned address;
    unsigned value;
    FILE *file;
    file = fopen(filename, "r");
    if(file == NULL) {
        #ifdef debug
        mvprintw(0, 0, "Couldn't load %s.", filename);
        #else
        printf("Couldn't load %s.\n", filename);
        #endif
        memory[255] = 1;
        return;
    }
    for(address = 0; address < 256; address++) {
        memory[address] = 0;
    }
    for(address = 0; address < 256; address++) {
        if(fscanf(file, " %d", &value) == EOF) {
            break;
        }
        memory[address] = value;
    }
    fclose(file);
    #ifdef debug
    mvprintw(0, 0, "%s loaded.", filename);
    #else
    printf("%s loaded.\n", filename);
    #endif
    return;
}

void monitor(unsigned *memory) {
    static unsigned char run        = 0;
                    char buffer     [256];
           unsigned      param1;
           unsigned      param2;
           unsigned      x;
           unsigned      y;
           unsigned      address;
                    char operation;
    operation = 0;
    y         = 0;
    for(x = 0; x < 16; x++) {
        for(y = 0; y < 16; y++) {
            address = y + 16 * x;
            move(x + 1, y);
            if(address == memory[0] ||
               address == memory[0] + 1) {
                addch('i');
            }
            else if(address == memory[memory[0]]) {
                addch('s');
            }
            else if(address == memory[memory[0] + 1]) {
                addch('d');
            }
            else {
                addch('.');
            }
        }
    }
    if(run != 0) {
        operation = getch();
        if(operation   == 'h' ||
           memory[255] != 0) {
            echo();
            curs_set(1);
            nodelay(stdscr, 0);
            run = 0;
        }
        return;
    }
    while(operation != 'c' &&
          run       == 0) {
        operation = mvgetch(17, 0);
        move(17, 0);
        clrtoeol();
        move(0, 0);
        clrtoeol();
        switch(operation) {
            case 's': {
                mvprintw(0, 0, "Set.");
                mvscanw(17, 0, "%d %d", &param1, &param2);
                memory[param1] = param2;
                break;
            }
            case 't': {
                mvprintw(0, 0, "Transfer from...");
                mvscanw(17, 0, "%d", &param1);
                move(17, 0);
                clrtoeol();
                move(0, 0);
                clrtoeol();
                mvprintw(0, 0, "... to...");
                mvscanw(17, 0, "%d", &param2);
                memory[param2] = memory[param1];
                move(0, 0);
                clrtoeol();
                mvprintw(0, 0, "Transferred from %d to %d.", param1, param2);
                break;
            }
            case 'p': {
                mvprintw(0, 0, "Print.");
                mvscanw(17, 0, " %d", &param1);
                move(0, 0);
                clrtoeol();
                mvprintw(0, 0, "Value: %d", memory[param1]);
                break;
            }
            case 'i': {
                mvprintw(0, 0, "Display instruction.");
                mvscanw(17, 0, "%d %d", &param1);
                mvprintw(0, 0, "%d:%d -> %d:%d\n", memory[param1],
                                                   memory[memory[param1]],
                                                   memory[param1 + 1],
                                                   memory[memory[param1 + 1]]);
                break;
            }
            case 'l': {
                mvprintw(0, 0, "Load a file.");
                mvscanw(17, 0, " %s", buffer);
                process_file(buffer, memory);
                break;
            }
            case 'r': {
                mvprintw(0, 0, "Running...\n");
                noecho();
                curs_set(0);
                nodelay(stdscr, 1);
                run = 1;
                break;
            }
            case 'c': {
                break;
            }
            default: {
                mvprintw(0, 0, "Caught: %d\n", operation);
                break;
            }
        }
        move(17, 0);
        clrtoeol();
        refresh();
    }
    mvprintw(0, 0, "%d:%d -> %d:%d\n", memory[memory[0]],
                                       memory[memory[memory[0]]],
                                       memory[memory[0] + 1],
                                       memory[memory[memory[0] + 1]]);
    refresh();
    return;
}

int main(int argc, char **argv) {
    unsigned memory[256] = { 0 };
    #ifdef debug
    initscr();
    mvprintw(0, 0, "Starting in debug mode.");
    #else
    process_file(argv[1], memory);
    #endif
    #ifdef debug
    while(1) {
    #else
    while(memory[255] == 0) {
    #endif
        #ifdef debug
        monitor(memory);
        if(memory[255] == 0) {
        #endif
        program_counter(memory);
        alu(memory);
        dereference(memory);
        branch(memory);
        print(memory);
        #ifdef debug
        }
        #endif
    }
    #ifdef debug
    endwin();
    #endif
    return 0;
}
